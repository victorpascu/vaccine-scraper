require("dotenv").config();
const puppeteer = require("puppeteer");

async function reloadLoop(page) {
  await page.waitForTimeout(60000);

  console.log("Reloading at " + new Date());
  try {
    await goToSchedulingPage(page);
    await goToCountyPage(page);
  } catch (err) {
    try {
      await login(page);
      await page.waitForTimeout(10000);
      await goToSchedulingPage(page);
      await page.waitForTimeout(10000);
      await goToCountyPage(page);
      await page.waitForTimeout(10000);
    } catch (err) {
      console.log(err);
      console.log("Failed to reload. Retrying in one minute.");
    }
  }

  reloadLoop(page);
}

async function goToSchedulingPage(page) {
  await page.goto(
    "https://programare.vaccinare-covid.gov.ro/#/appointment/new/" +
      process.env.GOV_RECIPIENT_ID
  );

  await page.waitForTimeout(3000);

  return page;
}

async function login(page) {
  console.log("Going to login page.");
  await page.goto("https://programare.vaccinare-covid.gov.ro/auth/login#/home");
  await page.waitForTimeout(10000);
  console.log("Waiting for password input to load.");
  await page.waitForSelector("input[type=password]");
  console.log("Found password input.");
  await page.type("input[name=username]", process.env.GOV_EMAIL);
  await page.type("input[type=password]", process.env.GOV_PASSWORD);
  await page.click("button");
  console.log("Logged in.");
  await page.waitForTimeout(10000);
}

async function goToCountyPage(page) {
  await page.click("autocomplete-county input");
  await page.click("mat-option:nth-child(" + process.env.COUNTY_OPTION + ")");
  await page.waitForTimeout(500);

  await page.click("autocomplete-locality input");
  await page.click("mat-option:nth-child(" + process.env.LOCALITY_OPTION + ")");
  await page.waitForTimeout(500);

  await page.click(
    "button.mat-focus-indicator.mat-raised-button.mat-button-base.mat-accent"
  );
  await page.waitForTimeout(2000);

  return page;
}

async function startBrowser(io = null) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.setRequestInterception(true);

  page.on("response", async (response) => {
    // programare.vaccinare-covid.gov.ro/auth/login?sessionExpired=true
    if (response.url().endsWith("sessionExpired=true")) {
      try {
        console.log("Session expired!");
      } catch (err) {
        console.log(err);
      }
    }
    //https://programare.vaccinare-covid.gov.ro/scheduling/api/centres?page=0&size=10&sort=,
    if (
      response
        .url()
        .startsWith(
          "https://programare.vaccinare-covid.gov.ro/scheduling/api/centres"
        )
    ) {
      try {
        let data = await response.json();
        let centersData = [];
        data.content.forEach((center) => {
          centersData = [...centersData, center];

          if (center.availableSlots > 0) {
            console.log("[ === AVAILABLE SLOTS === ]");
          }
          let currentDate = new Date();
          console.log(
            "[" +
              currentDate.getHours() +
              ":" +
              currentDate.getMinutes() +
              "]" +
              "[" +
              center.availableSlots +
              " slots] " +
              center.name +
              " - " +
              center.localityName +
              " - " +
              center.countyName
          );
        });
        // io.on('connection', function (client) {
        //   console.log('Client connected...');
        //   client.on('join', function (data) {
        //     console.log(data);
        //     client.emit('messages', 'Server connection established.');
        //   });
        // });
        io.emit("messages", centersData);
      } catch (err) {
        console.log(err);
      }
    }
  });

  page.on("request", (request) => {
    const path = request.url();
    if (path.includes(".svg")) {
      request.abort();
    } else if (request.resourceType() === "image") {
      // Save bandwidth by not loading images
      request.abort();
    }
    // Save bandwidth by not loading stylesheets
    else if (request.resourceType() === "stylesheet") {
      request.abort();
    } else {
      request.continue();
    }
  });

  return { browser, page };
}

async function getVaccineSlots(io = null) {
  const { browser, page } = await startBrowser(io);
  page.setViewport({ width: 1366, height: 768 });

  await login(page);
  //https://programare.vaccinare-covid.gov.ro/#/appointment/new/RECIPIENT_ID
  await goToSchedulingPage(page);

  await page.waitForTimeout(3000);
  // Set page filters
  await goToCountyPage(page);

  // Enable reloading
  await reloadLoop(page);
}

module.exports = { getVaccineSlots };
