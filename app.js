require("dotenv").config();
var express = require("express");
var app = express();
var server = require("http").createServer(app);
var io = require("socket.io")(server);
var vaccineSlots = require("./vaccineSlots");

app.use(express.static(__dirname + "/node_modules"));
app.get("/", function (req, res, next) {
  var countyFilter = process.env.COUNTY_NAME;
  res.sendFile(__dirname + "/index.html", {
    headers: { countyFilter: process.env.COUNTY_NAME },
  });
});

io.on("connection", function (client) {
  client.on("join", function (data) {
    // client.emit('messages', 'Server connection established.');
  });
});

vaccineSlots.getVaccineSlots(io);

server.listen(4200);
