# RO Vaccine availability scanner

## What it does

Scans the vaccine availability page roughly once every minute to see if there are any free slots, providing an audio notification if there are.

## How to use

You must have Node.js installed. You can run the following in a terminal open in the project folder.

Please configure your .env file by copying `.env.dist` to `.env` and filling out appropriate values.
You must set all of the values in your `.env` file correctly:

`GOV_EMAIL='you@gmail.com'`

`GOV_PASSWORD='strongpassword'`

`GOV_RECIPIENT_ID='1234567'` - you get this from the URL when you log in and try to make an appointment,
i.e.: https://programare.vaccinare-covid.gov.ro/#/appointment/new/1234567

`COUNTY_OPTION=14` - the number your preferred county has in the county dropdown. Ex - Cluj is 14.
`COUNTY_NAME='Cluj'` - should match the county name as displayed in the dropdown. Ex - Cluj

`LOCALITY_OPTION=130` - the number your preferred locality has in the locality dropdown. Ex - Cluj-Napoca is 130.

Install dependencies:
`npm install`

To run the server:
`node app.js`

To make page available via ngrok (run in separate terminal):
`ngrok http 4200`

You can access the scanner at:
`http://localhost:4200`

Notification sound from: https://freesound.org/people/FoolBoyMedia/sounds/234524/

To format new code:
`npx prettier --write .`
